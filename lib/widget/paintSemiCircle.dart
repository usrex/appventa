// This is the Painter class
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'dart:math' as math;

class MyPainter extends CustomPainter {
  Color data;
  double startAngle;

  MyPainter({this.data = Colors.red, this.startAngle});

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()..color = data;
    Offset offset;
    if ((math.pi * 4 / 2) == this.startAngle) {
      offset = new Offset(size.height / 2, 0);
    } else {
      offset = new Offset(size.height / 2, size.width);
    }
    canvas.drawArc(
      Rect.fromCircle(center: offset, radius: size.width / 2),
      /*Rect.zero(
        center: Offset(size.height / 2, size.width),
        height: size.height,
        width: size.width,
      ),*/
      //math.pi * 4 / 2,
      //math.pi,
      startAngle,
      math.pi,
      false,
      paint,
    );
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
