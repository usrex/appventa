import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:ventas/helper/RestVenta.dart';
import 'package:ventas/models/Product.model.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';

class AddPrdInit extends StatefulWidget {
  @override
  _AddPrdInitState createState() => _AddPrdInitState();
}

class _AddPrdInitState extends State<AddPrdInit> {
  RestProvider rest = RestProvider();
  TextEditingController _nmprod = TextEditingController();
  TextEditingController _desc = TextEditingController();
  TextEditingController _categoria = TextEditingController();
  TextEditingController _cant = TextEditingController();
  File _fileImage;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 36),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Registrar Producto",
              style: TextStyle(fontSize: 24),
            ),
            SizedBox(
              height: 32,
            ),
            TextField(
              controller: _nmprod,
              decoration: InputDecoration(
                labelText: "Nombre Prodcuto",
                prefixIcon: Icon(Icons.add_box),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            TextField(
              controller: _desc,
              decoration: InputDecoration(
                labelText: "Descripcion",
                prefixIcon: Icon(Icons.comment),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            TextField(
              controller: _categoria,
              decoration: InputDecoration(
                labelText: "Categoria",
                prefixIcon: Icon(Icons.add_box),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            TextField(
              controller: _cant,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                labelText: "Cantidad",
                prefixIcon: Icon(Icons.add_box),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text("Añadir Imagen"),
                IconButton(icon: Icon(Icons.camera), onPressed: ()=> getImagecamera()),
                IconButton(
                    icon: Icon(Icons.picture_in_picture),
                    onPressed: () => getImage()),
              ],
            ),
            SizedBox(
              height: 16,
            ),
            RaisedButton(
              padding: EdgeInsets.symmetric(horizontal: 25),
              onPressed: () async {
                String nombre = _nmprod.text.trim();
                String descripcion = _desc.text.trim();
                String categoria = _categoria.text.trim();
                int cantidad = int.parse(
                    _cant.text.trim().isEmpty ? 0 : _cant.text.trim());
                if (!(nombre.isEmpty &&
                    descripcion.isEmpty &&
                    categoria.isEmpty)) {
                  DateTime now = new DateTime.now();
                  print(now);
                  ProductModel tmp = ProductModel(
                      nombre: nombre,
                      descripcion: descripcion,
                      categoria: categoria,
                      cantidad_productos: cantidad,
                      url_image_product:
                          "https://i.pinimg.com/736x/02/eb/f6/02ebf6e77a2a6d9c77f23ca36e49b594.jpg",
                      fecha_registro: now.toString());

                  ProductModel resp = await rest.AddProduct(tmp);
                  Toast.show(
                    "Producto ${resp.nombre} agregado con exito",
                    context,
                    duration: Toast.LENGTH_LONG,
                    gravity: Toast.BOTTOM,
                  );
                }
              },
              shape: StadiumBorder(),
              color: Colors.red,
              child: Text("Guardar",style: TextStyle(color: Colors.white,),),
            ),
          ],
        ),
      ),
    );
  }

  Future getImage() async {
    File img = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _fileImage = img;
    });
  }
  Future getImagecamera() async {
    File img = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      _fileImage = img;
    });
  }
}
