import 'package:flutter/material.dart';
import 'package:ventas/helper/sharedPreference.dart';
import 'package:ventas/views/ListProduct.dart';
import 'package:ventas/views/ProductsHome.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _prefs = PreferenceProvider();
  String nombre;
  int _numerop = 0;
  //creamos las combinaciones de pantalla donde usamos una lista de widgets
  List<Widget> _pantallas = <Widget>[
    ListProducts(),
    ProductsPromo(),
    Text(
      'Posición 0: ',
      style: TextStyle(fontSize: 40),
    ),
    Text(
      'Posición 1: ',
      style: TextStyle(fontSize: 40),
    ),
  ];
  //funcion para recorrer las pantallas
  void _recorrido(int index) {
    setState(() {
      _numerop = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: _botonave(),
      body: _pantallas.elementAt(_numerop),
    );
  }

  Widget _body() {
    return FutureBuilder<String>(
      future: _prefs.getDataString(KeyList.USER_NAME),
      // ignore: missing_return
      builder: (context, AsyncSnapshot<String> snapshot) {
        if (snapshot.hasData) {
          //print("data" + snapshot.data);
          //return Text(snapshot.data);
          return Text("Name");
        } else {
          return Text("Not data");
        }
      },
    );
  }

  Widget _appBar() {
    return AppBar(
      backgroundColor: Colors.red,
      title: FutureBuilder<String>(
        future: _prefs.getDataString(KeyList.USER_NAME),
        // ignore: missing_return
        builder: (context, AsyncSnapshot<String> snapshot) {
          if (snapshot.hasData) {
            //print("data" + snapshot.data);
              //return Text(snapshot.data);
          return Text("Name");
          } else {
            return Text("Not data");
          }
        },
      ),
    );
  }

  Widget _botonave() {
    return BottomNavigationBar(
      
      backgroundColor: Colors.redAccent,
      //maenjamos los tipos de animacion
      type: BottomNavigationBarType.fixed,
      //cambiamos el color de los item (icono, texto)
      unselectedItemColor: Colors.black,
      //color de cuando se pone un evento
      fixedColor: Colors.white,
      //asignamos el numero de pantallas
      currentIndex: _numerop,
      //manejamos el recorrido
      onTap: _recorrido,
      //los botones deben ser mayores o igual a >=2
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
            icon: Icon(Icons.store), title: Text("Productos")),
        BottomNavigationBarItem(
          icon: Icon(Icons.list),
          title: Text("Promociones"),
        ),
      ],
    );
  }
}
