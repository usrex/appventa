import 'package:flutter/material.dart';
import 'package:ventas/helper/RestVenta.dart';
import 'package:ventas/views/AddProductInit.dart';
import 'package:ventas/views/AddTalla.dart';
import 'package:ventas/views/addColorsPrecio.dart';

class AddProduct extends StatefulWidget {
  @override
  _AddProductState createState() => _AddProductState();
}

class _AddProductState extends State<AddProduct> {
  
  int _index = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
      ),
      body: PageView(
        onPageChanged: (i) {
          print(i);
          setState(() {
            _index = i;
          });
        },
        controller: PageController(
          initialPage: _index,
          viewportFraction: 0.98,
        ),
        children: [
          AddPrdInit(),
          AddTallaProduct(),
          AddColorsPrecio(),
        ],
      ),
      bottomNavigationBar: Container(
        color: Colors.redAccent,
        height: 40,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            CircleAvatar(
              backgroundColor: _index == 0 ? Colors.white70 : Colors.white,
              radius: _index == 0 ? 15 : 10,
            ),
            CircleAvatar(
              backgroundColor: _index == 1 ? Colors.white70 : Colors.white,
              radius: _index == 1 ? 15 : 10,
            ),
            CircleAvatar(
              backgroundColor: _index == 2 ? Colors.white70 : Colors.white,
              radius: _index == 2 ? 15 : 10,
            ),
          ],
        ),
      ),
    );
  }
}
