import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:ventas/helper/RestVenta.dart';
import 'package:ventas/helper/sharedPreference.dart';
import 'package:ventas/models/Product.model.dart';
import 'package:ventas/views/CarritoPage.dart';

class ListProducts extends StatefulWidget {
 
  @override
  _ListProductsState createState() => _ListProductsState();
}

class _ListProductsState extends State<ListProducts> {
  List<String> tipos = <String>[
    'Productos',
    'Accesorios',
    'Calzados',
    'Lenceria',
    'Blusas de Dama',
    'Pantalones Casual Urbano'
  ];
  List _pantallas = [
    _Products(),
    _Products(categoria: "Accesorios"),
    _Products(categoria: 'Calzados'),
    _Products(categoria: 'Lenceria'),
    _Products(categoria: 'Blusas de Dama'),
    _Products(categoria: 'Pantalones Casual Urbano'),
  ];

  int _numerop = 0;
final _prefs = PreferenceProvider();
 
 
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: tipos.length,
      child: Scaffold(
        appBar: AppBar(
          title: FutureBuilder<String>(
        future: _prefs.getDataString(KeyList.USER_NAME),
        // ignore: missing_return
        builder: (context, AsyncSnapshot<String> snapshot) {
          if (snapshot.hasData) {
            //print("data" + snapshot.data);
              return Text(snapshot.data);
          //return Text("Name");
          } else {
            return Text("Not data");
          }
        },
      ),
          actions: [
            IconButton(
              icon: Icon(Icons.shopping_cart),
              onPressed: () {
                 Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => CarritoPage()),
                );
              },
            )
          ],
          backgroundColor: Colors.redAccent,
          bottom: TabBar(
            isScrollable: true,
            onTap: (index) {
              setState(() {
                _numerop = index;
                print(_numerop);
              });
            },
            tabs: [
              Container(
                width: 80,
                child: Tab(
                  icon: Icon(Icons.directions_car),
                  text: "${tipos[0]}",
                ),
              ),
              Container(
                width: 80,
                child: Tab(
                  icon: Icon(Icons.directions_transit),
                  text: "${tipos[1]}",
                ),
              ),
              Container(
                width: 80,
                child: Tab(
                  icon: Icon(Icons.directions_bike),
                  text: "${tipos[2]}",
                ),
              ),
              Container(
                width: 80,
                child: Tab(
                  icon: Icon(Icons.directions_bike),
                  text: "${tipos[3]}",
                ),
              ),
              Container(
                width: 80,
                child: Tab(
                  icon: Icon(Icons.directions_bike),
                  text: "${tipos[4]}",
                ),
              ),
              Container(
                width: 80,
                child: Tab(
                  icon: Icon(Icons.directions_bike),
                  text: "${tipos[5]}",
                ),
              ),
            ],
          ),
        ),
        body: _pantallas.elementAt(_numerop),
      ),
    );
  }
}

class _Products extends StatefulWidget {
  final String categoria;
  _Products({this.categoria});

  @override
  __ProductsState createState() => __ProductsState();
}

class __ProductsState extends State<_Products> {
  RestProvider rest = RestProvider();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder(
        future: widget.categoria == null
            ? rest.getProduct()
            : rest.getProductCategory(widget.categoria),
        builder: (context, AsyncSnapshot snapshot) {
          print("******cat");
          print(widget.categoria);
          if (snapshot.hasData) {
            List<ProductModel> data = snapshot.data;
            return ListView.builder(
              itemCount: data.length,
              itemBuilder: (context, i) {
                return _cardProduct(
                    title: "${data[i].nombre}",
                    descr: "${data[i].descripcion}",
                    networkimg: "${data[i].url_image_product}",
                    precio: "${data[i].cantidad_productos}");
              },
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  Widget _cardProduct(
      {String title = "Product",
      String networkimg,
      String descr,
      String precio = "0.0"}) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16),
      child: InkWell(
        onTap: () {},
        child: Card(
          child: Column(
            children: [
              Text(
                "${title}",
                style: TextStyle(fontSize: 24),
              ),
              Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.symmetric(
                        vertical: 16,
                        horizontal: 12,
                      ),
                      height: 250,
                      width: 250,
                      child: Image.network(
                        "${networkimg}",
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.only(right: 8),
                      child: Column(
                        children: [
                          Text("Descipcion"),
                          Text(
                            "${descr}",
                            style: TextStyle(fontSize: 10),
                          ),
                          SizedBox(
                            height: 12,
                          ),
                          Row(
                            children: [
                              Icon(Icons.attach_money),
                              Text("${precio} Bs")
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  RaisedButton(
                    color: Colors.red,
                    shape: StadiumBorder(),
                     onPressed: () {
                      showDialog(context: context,
                      child:AlertDialog(content: Text("¿ Desea Agregar el Producto al Carrito ?"),
                      actions: [
                        FlatButton(onPressed: (){}, child: Text("Aceptar")),
                        FlatButton(onPressed: (){}, child: Text("Cancelar")),
                      ],));
                    },
                    child: Container(
                      width: 100,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Icon(
                            Icons.add,
                            color: Colors.white,
                          ),
                          Text(
                            "Agregar",
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
