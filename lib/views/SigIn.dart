import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:ventas/helper/RestVenta.dart';
import 'package:ventas/models/User.model.dart';

class SigIn extends StatefulWidget {
  @override
  _SigInState createState() => _SigInState();
}

class _SigInState extends State<SigIn> {
  TextEditingController _nombre = TextEditingController();
  TextEditingController _ape = TextEditingController();
  TextEditingController _mail = TextEditingController();
  TextEditingController _pass = TextEditingController();

  RestProvider _rest = RestProvider();
  bool obscure = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.redAccent[700],
      body: SafeArea(
        child: Center(child: _body()),
      ),
    );
  }

  Widget _body() {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Leyvas Fashion \n      Registro",
              style: TextStyle(fontSize: 36, color: Colors.white),
            ),
            SizedBox(
              height: 15,
            ),
            Hero(
              tag: 'registro',
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 22.0, horizontal: 16.0),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 32.0, vertical: 8.0),
                  child: _formRegistro(),
                ),
                decoration: BoxDecoration(
                    color: Colors.white60,
                    borderRadius: BorderRadius.circular(10.2)),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _formRegistro() {
    return Column(
      children: [
        Container(
          width: 190.0,
          height: 190.0,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.black87,
            image: false
                ? DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/leyv.jpeg'),
                  )
                : null,
          ),
        ),
        _formText(
          inputType: TextInputType.text,
          labelText: "nombre",
          icon: Icons.people_outline,
          controller: _nombre,
        ),
        _formText(
            inputType: TextInputType.text,
            labelText: "Apellidos",
            icon: Icons.people_outline,
            controller: _ape),
        _formText(
          inputType: TextInputType.emailAddress,
          labelText: "Correo",
          icon: Icons.email_outlined,
          controller: _mail,
        ),
        _formText(
            obscure: obscure ? true : false,
            inputType: TextInputType.visiblePassword,
            labelText: "Password",
            icon: obscure ? Icons.lock_outlined : Icons.lock_open_outlined,
            controller: _pass,
            callback: () {
              setState(() {
                obscure = !obscure;
              });
            }),
        RaisedButton(
          child: Text(
            "Sign In",
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () async {
            String nombre = _nombre.text.trim();
            String apellido = _ape.text.trim();
            String correo = _mail.text.trim();
            String password = _pass.text.trim();

            if (nombre.isEmpty ||
                apellido.isEmpty ||
                correo.isEmpty ||
                password.isEmpty) {
              Toast.show("Relle todos los datos del formulario", context,
                  duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
            } else {
              UserModel usertmp = UserModel(
                  nombre: nombre,
                  apellidos: apellido,
                  correo: correo,
                  pasword: password,
                  rol_usuario: 'Usuario',
                  url_image: 'https://i.pinimg.com/474x/c6/94/8f/c6948f28bfc3757f0970e4cf5bf67126.jpg');

              var resp = await _rest.registerUser(usertmp);
              if (resp == 'usuario agregado con exito') {
                Navigator.pushNamed(context, '/sigUp');
              }
              Toast.show(resp, context,
                  duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
              print("********************");
              print(resp);
              print("********************");
            }
          },
          shape: StadiumBorder(),
          color: Colors.redAccent[700],
        )
      ],
    );
  }

  Widget _formText(
      {String labelText,
      @required TextInputType inputType,
      @required IconData icon,
      bool obscure = false,
      @required TextEditingController controller,
      VoidCallback callback = null}) {
    return TextField(
      obscureText: obscure,
      controller: controller,
      keyboardType: inputType,
      maxLines: 1,
      decoration: InputDecoration(
        prefixIcon:  IconButton(
          onPressed: callback,
          icon: Icon(
            icon,
            color: Colors.black,
          ),
        ),
        labelText: labelText,
        labelStyle: TextStyle(color: Colors.black87),
      ),
    );
  }
}
