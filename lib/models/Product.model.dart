class ProductModel{
        int id_producto;
        String nombre;
        String descripcion;
        int cantidad_productos;
        String fecha_registro;
        String categoria;
        String url_image_product;

        ProductModel({
          this.nombre,
          this.descripcion,
          this.cantidad_productos,
          this.fecha_registro,
          this.categoria,
          this.url_image_product
        });

  
  ProductModel.fromJson(Map<String, dynamic> json) {
    this.id_producto = json['id_producto'];
    this.nombre = json['nombre'];
    this.descripcion = json['descripcion'];
    this.cantidad_productos = json['cantidad_productos'];
    this.fecha_registro = json['fecha_registro'];
    this.categoria = json['categoria'];
    this.url_image_product = json['url_image_product'];
  }

  Map<String, dynamic> toJsonRegister() {
    return {
      'nombre': this.nombre,
      'descripcion': this.descripcion,
      'cantidad_productos': this.cantidad_productos,
      'fecha_registro': this.fecha_registro,
      'categoria': this.categoria,
      'url_image_product': this.url_image_product,
    };
  }
}