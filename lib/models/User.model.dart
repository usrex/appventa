class UserModel {
  int id_usuario;
  String nombre;
  String apellidos;
  String correo;
  String pasword;
  String rol_usuario;
  String url_image;

  UserModel(
      {this.id_usuario,
      this.nombre,
      this.apellidos,
      this.correo,
      this.pasword,
      this.rol_usuario,
      this.url_image});

  UserModel.fromJson(Map<String, dynamic> json) {
    this.id_usuario = json['id_usuario'];
    this.nombre = json['nombre'];
    this.apellidos = json['apellidos'];
    this.correo = json['correo'];
    this.pasword = json['pasword'];
    this.rol_usuario = json['rol_usuario'];
    this.url_image = json['url_image'];
  }

  Map<String, dynamic> toJsonRegister() {
    return {
      'id_usuario': this.id_usuario,
      'nombre': this.nombre,
      'apellidos': this.apellidos,
      'correo': this.correo,
      'pasword': this.pasword,
      'rol_usuario': this.rol_usuario,
      'url_image': this.url_image,
    };
  }

  Map<String, dynamic> toJsonLogin() {
    return {'correo': this.correo, 'pasword': this.pasword};
  }
}
